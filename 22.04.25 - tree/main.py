import random
import matplotlib.pyplot as plt
import networkx as nx
from networkx.drawing.nx_pydot import graphviz_layout


def get_node_id(node_id = {"n": -1}):
    node_id["n"] += 1
    return node_id["n"]

def gen_binary_tree(max_levels=3):
    nodes = []
    edges = []
    level = 0
    node_id = 0
    nodes.append(node_id)
    parent_id = node_id
    _gen_tree(parent_id, nodes, edges, level, max_levels)
    return nodes, edges

def _gen_tree(parent_id, nodes, edges, level, max_levels):
    level +=1
    if level >= max_levels:
        return
    for _ in range(2):
        if bool(random.getrandbits(1)):
            node_id = get_node_id()
            edges.append((parent_id, node_id))
            nodes.append(node_id)
            _gen_tree(node_id, nodes, edges, level, max_levels)


nodes, edges = gen_binary_tree(5)

tree = nx.Graph()
tree.add_nodes_from(nodes)
tree.add_edges_from(edges)

plt.figure(figsize=(10,10))
pos = graphviz_layout(tree, prog="dot")
nx.draw(tree, pos, with_labels=True,font_size=22)
plt.show()
