import os
import time
import random


def hash_crc(line: str) -> int:
    hsh = 0
    for i in line:
        hh = hsh & 0xf8000000
        hsh = hsh << 5
        hsh = hsh ^ (hh >> 27)
        hsh = hsh ^ ord(i)
    return(hsh)

def hash_pjw(line: str) -> int:
    hsh = 0
    for i in line:
        hsh = (hsh << 4) + ord(i)
        g = hsh & 0xf0000000
        if (g != 0):
            hsh = hsh^(g >> 24)
            hsh = hsh^g
    return(hsh)

def hash_buz(line: str) -> int:
    hsh = 0
    for i in line:
        hh = hsh & 0x80000000
        hsh = hsh << 1
        hsh = hsh ^ (hh >> 31)
        random.seed(ord(i))
        hsh = hsh ^ random.randint(0, 1000)
    return(hsh)

def find_duplicates(files: list[str], hash_function: callable) -> list[str]:
    start_time = time.time()
    fls = []
    for fl_name in files:
        fl = open("./out/"+fl_name, 'r')
        fls.append(fl.read())
        fl.close()
    hashs = []
    for fl in fls:
        hashs.append(hash_function(fl))
    dups = []
    dups2 = []
    for i in range(len(hashs)):
        if (i in dups):
            continue
        for j in range(i+1, len(hashs)):
            if (hashs[i] == hashs[j]):
                dups.append(j)
                dups2.append(files[i]+'-'+files[j])
    elapsed_time = time.time() - start_time
    dups = [str(elapsed_time)] + [str(len(dups))] + dups
    dups2 = [str(elapsed_time)] + [str(len(dups2))] + dups2
    return(dups2)

def save_crc(fls):
    dups = find_duplicates(fls, hash_crc)
    fl = open("crc.txt", 'w')
    for i in dups:
        fl.write(i+'\n')
    fl.close()

def save_pjw(fls):
    dups = find_duplicates(fls, hash_pjw)
    fl = open("pjw.txt", 'w')
    for i in dups:
        fl.write(i+'\n')
    fl.close()

def save_buz(fls):
    dups = find_duplicates(fls, hash_buz)
    fl = open("buz.txt", 'w')
    for i in dups:
        fl.write(i+'\n')
    fl.close()

def save_hash(fls):
    dups = find_duplicates(fls, hash)
    fl = open("hash.txt", 'w')
    for i in dups:
        fl.write(i+'\n')
    fl.close()


path = input("Директория с файлами: ")
print()
print("Далее будут предложены функции хэширования.")
print("Можете выбрать одну, прописав её название, или сразу все, не написав ничего из перечисленного.")
mode = input("crc, pjw, buz, hash: ")
fls = os.listdir(path)
if (mode == "crc"):
    save_crc(fls)
    print()
    print("Файл с ответом создан в директории с файлом программы.")
    print("Первая строчка - время выполнения. Вторая строчка - количество дупликатов.")
    print("Остальные строчки - список совпадений.")
elif (mode == "pjw"):
    save_pjw(fls)
    print()
    print("Файл с ответом создан в директории с файлом программы.")
    print("Первая строчка - время выполнения. Вторая строчка - количество дупликатов.")
    print("Остальные строчки - список совпадений.")
elif (mode == "buz"):
    save_buz(fls)
    print()
    print("Файл с ответом создан в директории с файлом программы.")
    print("Первая строчка - время выполнения. Вторая строчка - количество дупликатов.")
    print("Остальные строчки - список совпадений.")
elif (mode == "hash"):
    save_hash(fls)
    print()
    print("Файл с ответом создан в директории с файлом программы.")
    print("Первая строчка - время выполнения. Вторая строчка - количество дупликатов.")
    print("Остальные строчки - список совпадений.")
else:
    save_crc(fls)
    save_pjw(fls)
    save_buz(fls)
    save_hash(fls)
    print()
    print("Файлы с ответами созданы в директории с файлом программы.")
    print("Первая строчка каждого файла - время выполнения определённой функцией.")
    print("Вторая строчка каждого файла - количество дупликатов.")
    print("Остальные строчки в каждом файле - списки совпадений.")
