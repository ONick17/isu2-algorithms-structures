#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <filesystem>

//using namespace std;

std::vector<int> read_file(std::string path){
    std::ifstream in(path);
    std::string line;
    std::vector<int>result;
    while(getline(in, line)){
        result.push_back(stoi(line));
    };
    for (auto it = result.begin(); it!=result.end(); it++){
        std::cout<< "result "<<*it<<std::endl;
    };
    return result;
};

void search(std::string path, std::vector<int>* result) {
    for (const auto &entry: std::filesystem::directory_iterator(path)) {
        if (entry.is_regular_file()) {
            
        }
        else {
            search(entry.path().u8string(), result);
        }
    }
}

int main(){
    std::string path = "data";
    std::vector<int>* result;
    search(path, result);
    std::cout << (*result)[1];
    //for (std::vector<int>::iterator iter = result.begin(); iter != result.end(); iter++) {}
    std::cout << "EVERYTHING IS GOOD" << std::endl;
    system("pause");
    return 0;
}