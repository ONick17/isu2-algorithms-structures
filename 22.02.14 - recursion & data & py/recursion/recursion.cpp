#include <iostream>
#include <chrono>

using namespace std;

int sum(int* array, int n) {
    int result = 0;
    for(int i=0; i < n; i++) {
        result += array[i];
    }
    return result;
}

int rsum(int* array, int n) {
    if (n <= 0) {
        return 0;
    }
    return array[n] + rsum(array, n-1);
}

int main()
{
    const int n = 100000;
    int array[n];
    for (int i=0; i < n; i++) {
        array[i] = 1;
    }

    auto t1 = std::chrono::high_resolution_clock::now();
    // int result = sum(array, n);
    
    int result = rsum(array, n);
    auto t2 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> elapsed = t2 - t1;
    
    std::cout << "Result = " << result << std::endl;
    std::cout << "Elapsed = " << elapsed.count() << "ms" << std::endl;
    system("pause");
    return 0;
}