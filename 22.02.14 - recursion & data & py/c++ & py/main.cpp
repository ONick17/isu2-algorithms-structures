#include <iostream>
#include <vector>
#include <cmath>

int main()
{
  std::vector<float> vec;
  int s;
  for (int i=0; i<=7; i++) {
    s = std::ceil(std::pow(10, i));
    vec.clear();
    for (int j=0; j<=s; j++) {
      vec.push_back(j);
    }
    std::cout << s << " " << sizeof(std::vector<float>) + sizeof(float) * vec.capacity() << std::endl;
  }
  std::cout << std::endl;
  system("pause");
  return 0;
}