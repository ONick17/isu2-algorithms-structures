import matplotlib.pyplot as plt

data = []
while  x := input().split():
    data.append(x[0])
    data.append(x[1])

x = list(map(float, data[::2]))
y = list(map(float, data[1::2]))
y2 = [i * 4 for i in x]

plt.plot(x, y)
plt.plot(x, y2, "--")
plt.show()
