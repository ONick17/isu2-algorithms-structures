def hash_find(txt):
    txt_arr = []
    for i in range(len(txt)):
        txt_arr.append(bin(ord(txt[i]))[2:])
    hsh = ""
    for i in range(len(txt_arr[0])):
        num = 0
        for j in txt_arr:
            num += int(j[i]) % 2
        num %= 2
        hsh += str(num)
    return(hsh)

txt = "asdqwerty"
hsh = hash_find(txt)
print("hash =", hsh)
print()

def hash_find2(txt):
    txt_arr = []
    for i in range(len(txt)):
        txt_arr.append(ord(txt[i]))
    hsh = ""
    ln = 6
    for i in range(7):
        num = 0
        for j in txt_arr:
            num += (j >> (ln - i)) % 2
        num %= 2
        hsh += str(num)
    return(hsh)

hsh = hash_find2(txt)
print("hash =", hsh)
print()
