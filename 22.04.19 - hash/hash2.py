def error_check(txt):
    txt = txt.split(' ')
    chk = int(txt[-1])
    cnt = 0
    for i in range(len(txt)-1):
        num = bin(int(txt[i])).count('1')
        cnt += num
    if cnt%2 == chk:
        return False
    else: return True


with open("out.txt") as fl:
    txts = fl.readlines()
cnt = 0
for i in txts:
    if error_check(i):
        cnt += 1
print(round(cnt/len(txts)*100), '%', sep='')
