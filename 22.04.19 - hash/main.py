import matplotlib.pyplot as plt
import math
import time


def distance(phi1, lam1, phi2, lam2, rad = 6371):
    phi1 = math.radians(phi1)
    phi2 = math.radians(phi2)
    lam1 = math.radians(lam1)
    lam2 = math.radians(lam2)
    a = math.sin((phi2-phi1)/2)+math.cos(phi1)*math.cos(phi2)*(math.sin((lam2-lam1)/2)**2)
    b = 2*rad*math.asin(a)
    return b
    #return 2*rad*math.asin(math.sqrt(math.sin((phi2-phi1)/2)+math.cos(phi1)*math.cos(phi2)*(math.sin((lam2-lam1)/2)**2)))

def thin_grid(points, min_distance=1000):
    result = []
    p = points.pop()
    result.append(p)
    while points:
        p2 = points.pop()
        for p1 in result:
            if (distance(p1[0], p1[1], p2[0], p2[1])) < min_distance:
                break
        else:
            result.append(p2)
    return result

def memorize(func):
    cache = dict()
    def wrapped(*args, **kwargs):
        print(f"Called at {time.time()}")
        param_hash = hash((args, tuple(kwargs.items())))
        if param_hash in cache:
            print("Get result from hash")
            return cache[param_hash]
        result = func(*args, **kwargs)
        cache[param_hash] = result
        return result
    return wrapped

def get_roi(lats, lons, vals, roi):
    result = []
    for lat, lon, val in zip(lats, lons, vals):
        if roi[0] < lon < roi[1] and roi[2] < lat < roi[3]:
            result.append([lat, lon, val])
    return result

def query(date: str, roi: tuple[float], min_distance: int=1000):
    with open(date, "r") as f:
        lats = []
        lons = []
        vals = []
        for line in f.readlines()[1:]:
            lat, lon, val = list(map(float, line.split()))
            lats.append(lat)
            lons.append(lon)
            vals.append(val)
        roi = get_roi(lats, lons, vals, roi)
        result = thin_grid(roi, min_distance)
        return result


t = time.perf_counter()
result = query("2020-01-01.dat", (-100, -80, 28, 45))
print(f"Elapsed: {time.perf_counter() - t}")

t = time.perf_counter()
result = query("2020-01-01.dat", (-100, -80, 28, 45))
print(f"Elapsed: {time.perf_counter() - t}")

print(distance(0, 0, 0, 180))
