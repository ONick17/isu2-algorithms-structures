#include "math.h"
#include "utils.h"
#include "iostream"
using namespace std;

int main(int argc, char *argv[]) {
    if (argc != 5) {
        cout << "ERROR" << endl;
    } else {
        float *coords = read_argv(argc, argv);
        cout << distance(coords[0], coords[1], coords[2], coords[3]) << endl;
    }
    system("pause");
    return 0;
}