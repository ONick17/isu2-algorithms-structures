import json
import pathlib

def make_dirs(destination, dirs, spaces=""):
    spaces += "   "
    for key, item in dirs.items():
        dest = destination / key
        print(f"{spaces}Create directory {dest}")
        dest.mkdir(exist_ok=True)
        for item_ in item:
            if isinstance(item_, dict):
                make_dirs(dest, item_, spaces)
            else:
                print(f"{spaces}Create file {dest / item_}")
                with (dest / item_).open("w") as f:
                    f.write("1")

def check_dirs(destination, dirs):
    for key, item in dirs.items():
        dest = destination / key
        if not dest.exists():
            return False
        for item_ in item:
            if isinstance(item_, dict):
                if not check_dirs(dest, item_):
                    return False
            else:
                dest2 = dest / item_
                if not dest2.exists():
                    return False
    return True
    
dirs = json.load(open("dirs.json", "r"))
make_dirs(pathlib.Path("."), dirs)
print(check_dirs(pathlib.Path("."), dirs))
