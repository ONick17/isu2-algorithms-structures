#include <iostream>

struct Array {
    public:
        int *A;
        int size;
        int length;

    void create(int size) {
        A = (int*) malloc(size * sizeof(int));
        length = 0;
    }

    void display() {
        for (int i = 0; i<length; i++){
            std::cout << A[i] << ' ';
        }
    }

    void append(int value) {
        if (length < size) {
            A[length] = value;
        }
    }

    void insert(int index, int value) {
        if (index >= 0 & index < length){
            for (int i=length; i>index; i--) {
                A[i] = A[i-1];
            }
            A[index] = value;
            length++;
        }
    }

    int del(int index) {
        int x = 0;
        if (index >= 0 && index < length) {
            x = A[index];
            for (int i=index; i < length-1; i++) {
                A[i] = A[i+1];
            }
            length--;
            return x;
        }
        return -1;
    }

    int search_transposition(int value) {
        for (int i = 0; i < length; i++) {
            if (A[i] == value) {
                std::swap(A[i], A[0]);
                return i;
            }
        }
        return -1;
    }

    int bin_search(int value) {
        int low, mid, high;
        low = 0;
        high = length - 1;
        while (low <= high) {
            mid = (low + high) / 2;
            if (A[mid] == value) {
                return mid;
            } else if (value < A[mid]) {
                high = mid;
            } else {
                low = mid;
            }
        }
        return -1;
    }

    void sort() {
        for (int i = 0; i < length-1; i++) {
            for (int j = 0; j < length-1; j++) {
                if (A[j] > A[j+1]) {
                    std::swap(A[j], A[j+1]);
                }
            }
        }
    }

    int max() {
        int max = A[0];
        for (int i = 1; i < length; i++) {
            if (A[i] > max) {
                max = A[i];
            }
        }
        return max;
    }

    int min() {
        int min = A[0];
        for (int i = 1; i < length; i++) {
            if (A[i] < min) {
                min = A[i];
            }
        }
        return min;
    }

    int sum() {
        int sum = 0;
        for (int i = 0; i < length; i++) {
            sum += A[i];
        }
        return sum;
    }

    float mean() {
        return (float)sum() / length;
    }

    void reverse() {
        int *B = (int *) malloc(length * sizeof(int));
        int i, j;
        for (i = length-1, j = 0; i >= 0; i--, j++) {
            B[j] = A[i];
        }
        for (i = 0; i < length; i++) {
            A[i] = B[i];
        }
    }

     void reverse2() {
        int i, j;
        for (i = 0, j = length - 1; i < j; i++, j--) {
            std::swap(A[i], A[j]);
        }
    }

    void reverse(int start, int stop) {
        stop--;
        for (; start < stop; start++, stop--) {
            std::swap(A[start], A[stop]);
        }
    }

    void roll(int n) {
        reverse(0, length);
        reverse(0, n);
        reverse(n, length);
    }

    void insert_sorted(int value) {
        if (length == size) {
            return;
        }
        int i = length - 1;
        while (i >= 0 && A[i] > value) {
            A[i+1] = A[i];
            i--;
        }
        A[i+1] = value;
        length++;
    }

    Array union_op(Array other) {
        int i, j, k;
        i = j = k = 0;
        Array result;
        result.create(length + other.length);
        while (i < length && j < other.length) {
            if (A[i] < other.A[j]) {
                result.A[k++] = A[i++];
            } else if (A[i] > other.A[j]) {
                result.A[k++] = other.A[j++];
            } else {
                result.A[k++] = A[i++];
                j++;
            }
        }

        for (; i < length; i++) {
            result.A[k++] = A[i];
        }

        for (; j < length; j++) {
            result.A[k++] = other.A[j];
        }

        result.length = k;
        return result;
    }

    Array intersection_op(Array other) {
        int i, j, k;
        i = j = k = 0;
        Array result;
        result.create(length + other.length);
        while (i < length && j < other.length) {
            if (A[i] < other.A[j]) {
                i++;
            } else if (A[i] > other.A[j]) {
                j++;
            } else {
                result.A[k++] = A[i++];
                j++;
            }
        }

        result.length = k;
        return result;
    }

    Array difference_op(Array other) {
        int i, j, k;
        i = j = k = 0;
        Array result;
        other.sort();
        result.create(length + other.length);
        while (i < length && j < other.length) {
            if (A[i] < other.A[j]) {
                result.A[k++] = A[i++];
            } else if (A[i] > other.A[j]) {
                j++;
            } else {
                i++;
                j++;
            }
        }

        for (; i < length; i++) {
            result.A[k++] = A[i];
        }

        result.length = k;
        return result;
    }
};

int main() {
    int raw_array[10] = {4, 5, 2, 1, 3};
    Array arr = {raw_array, 10, 5};
    /*Array arr;
    arr.create(10);
    for (int i=0; i<5; i++) {
        arr.A[i] = i;
    }*/
    arr.display();
    std::cout << std::endl;
    int raw_array2[10] = {4, 5, 6, 4, 5};
    Array arr2 = {raw_array2, 10, 5};
    arr2.display();
    std::cout << std::endl;
    Array arr3 = arr.union_op(arr2);
    arr3.display();
    std::cout << std::endl;
    arr2.display();
    return 0;
}