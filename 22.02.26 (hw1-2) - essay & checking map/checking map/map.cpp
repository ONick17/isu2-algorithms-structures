#include<iostream>
#include<map>
#include<cmath>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int main(){
    std::map<int, int> mp;
    int cnt = 0;
    clock_t start, end;

    for (int i=0; i<=6; i++){
        cnt = ceil(pow(10,i));
        start = clock();
        for (int a = 0, b = 1; a<=cnt; a++, b++){ 
            mp.insert(std::pair<int, int>(a, b));  
        }
        end = clock();
        std::cout << cnt << ' ' << sizeof(mp) + mp.size()*sizeof(std::map<int, int>) << ' ' << ((double) end - start) / ((double) CLOCKS_PER_SEC) << ' ';
        mp.clear();
    }

    //system("pause");
    return 0;
}
