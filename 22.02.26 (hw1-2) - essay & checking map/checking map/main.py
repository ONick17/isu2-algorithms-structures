import matplotlib.pyplot as plt

inp = input().split()
x = list(map(float, inp[ :21:3]))
y = list(map(float, inp[1:21:3]))
plt.plot(x, y, label="c++")

x = list(map(float, inp[21: :3]))
y = list(map(float, inp[22: :3]))
plt.plot(x, y, label="python")

plt.xlabel("Number of values")
plt.ylabel("Weight")
plt.legend()
plt.show()

plt.clf()

x = list(map(float, inp[ :21:3]))
y = list(map(float, inp[2:21:3]))
plt.plot(x, y, label="c++")

x = list(map(float, inp[21: :3]))
y = list(map(float, inp[23: :3]))
plt.plot(x, y, label="python")

plt.xlabel("Number of values")
plt.ylabel("Time")
plt.legend()
plt.show()