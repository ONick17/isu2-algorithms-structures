import sys
import time

cnt = 0
for i in range(7):
    d = {}
    cnt = 10**i
    tm = time.perf_counter()
    for j in range(cnt):
        d[j] = j+1
    print(cnt, sys.getsizeof(d), time.perf_counter() - tm, end=" ")
