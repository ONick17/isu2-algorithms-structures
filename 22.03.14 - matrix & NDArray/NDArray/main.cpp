#include <iostream>
#include <random>
#include <string> 

class NDArray {
    private:
        int* arr;
        int shape[2];
        int size;

    public:
        NDArray(int y, int x, int fill) {
            shape[0] = y;
            shape[1] = x;
            size = x * y;
            arr = new int[size];
            filling(fill);
        }

        int getX() {
            return shape[1];
        }

        int getY() {
            return shape[0];
        }

        //задание 1
        NDArray(int y, int x) {
            shape[0] = y;
            shape[1] = x;
            size = x * y;
            arr = new int[size];
        }

        void empty(int y, int x) {
            shape[0] = y;
            shape[1] = x;
            size = x * y;
            arr = new int[size];
        }

        void empty() {
            arr = new int[size];
        }

        //задание 2
        void zeroes(int y, int x) {
            shape[0] = y;
            shape[1] = x;
            size = x * y;
            filling(0);
        }

        void zeroes() {
            filling(0);
        }

        //задание 3
        void ones(int y, int x) {
            shape[0] = y;
            shape[1] = x;
            size = x * y;
            filling(1);
        }

        void ones() {
            filling(1);
        }

        //задание 4
        void randoms(int min, int max, int y, int x) {
            shape[0] = y;
            shape[1] = x;
            size = x * y;
            for (int i = 0; i < size; i++) {
                arr[i] = rand()%max + min;
            };
        }

        void randoms(int min, int max) {
            for (int i = 0; i < size; i++) {
                arr[i] = rand()%max + min;
            };
        }

        //задание 5
        const NDArray operator+(const NDArray &other_arr) {
            return operators('+', other_arr);
        }
        const NDArray operator-(const NDArray &other_arr) {
            return operators('-', other_arr);
        }
        const NDArray operator*(const NDArray &other_arr) {
            return operators('*', other_arr);
        }
        const NDArray operator/(const NDArray &other_arr) {
            return operators('/', other_arr);
        }

        //задание 6
        NDArray matmul(NDArray& other) {
            if (shape[1] == other.shape[0]) {
                NDArray new_arr (shape[0], other.shape[1]);
                int val;
                for (int i = 0; i < shape[0]*other.shape[1]; i++) {
                    val = 0;
                    int new_x = i % other.shape[1];
                    int new_y = i / other.shape[1];
                    for (int j = 0; j < shape[1]; j++) {
                        val += arr[xy_to_index(j, new_y)] * other.arr[other.xy_to_index(new_x, j)];
                    }
                    new_arr.arr[i] = val;
                }
                return new_arr;
            } else {
                
                throw "Error: wrong shape";
            }
        }

        //задание 7
        NDArray transpone() {
            NDArray new_arr(shape[1], shape[0]);
            for (int i = 0; i < shape[0]; i++) {
                for (int j = 0; j < shape[1]; j++) {
                    new_arr.arr[new_arr.xy_to_index(i, j)] = arr[xy_to_index(j, i)];
                }
            }
            return new_arr;
        }

        //задание 8
        NDArray sum(int lvl) {
            return sxn_check("sum", lvl);
        }
        NDArray min(int lvl) {
            return sxn_check("min", lvl);
        }
        NDArray max(int lvl) {
            return sxn_check("max", lvl);
        }

        int min() {
            return sxn_check("min");
        }
        int max() {
            return sxn_check("max");
        }

        NDArray mean(int lvl) {
            NDArray new_array = sum(lvl);
            for (int i = 0; i < new_array.size; i++) {
                new_array.arr[i] /= shape[lvl];
            }
            return new_array;
        }
        int mean() {
            return sum()/size;
        }

        //побочное
        std::string display() {
            std::string ans;
            for (int y = 0; y < shape[0]; y++) {
                for (int x = 0; x < shape[1]; x++) {
                    ans += std::to_string(arr[xy_to_index(x, y)]) + " ";
                }
            ans += '\n';
            }
            return ans;
        }

    protected:
        void filling(int a) {
            for (int i = 0; i < size; i++) {
                arr[i] = a;
            }
        }

        NDArray operators(char oper, const NDArray other) {
            if ((shape[0] == other.shape[0]) && (shape[1] == other.shape[1])) {
                NDArray new_arr(shape[0], shape[1]);
                for (int i = 0; i < size; i++) {
                    switch (oper) {
                    case '+':
                        new_arr.arr[i] = arr[i] + other.arr[i];
                        break;
                    case '-':
                        new_arr.arr[i] = arr[i] - other.arr[i];
                        break;
                    case '*':
                        new_arr.arr[i] = arr[i] * other.arr[i];
                        break;
                    case '/':
                        new_arr.arr[i] = arr[i] / other.arr[i];
                        break;
                    default:
                        throw "Error: unknown operator";
                        break;
                    }
                }
                return new_arr;
            } else {
                if ((shape[0] != other.shape[0]) && (shape[1] == other.shape[1])) {
                    throw "Error: wrong number of lines";
                } else {
                    if  ((shape[0] == other.shape[0]) && (shape[1] != other.shape[1])) {
                        throw "Error: wrong number of columns";
                    } else {
                        throw "Error: wrong shape";
                    }
                }
            }
        }

        int xy_to_index(int x, int y) {
            return (y*shape[1] + x);
	    }

        int sum() {
            return sxn_check("sum");
        }

        NDArray sxn_check(std::string tp, int lvl) {
            if (lvl == 0) {
                int val;
                NDArray new_arr(1, shape[1]);
                if (tp == "sum") {
                    for (int x = 0; x < shape[1]; x++) {
                        if (tp == "sum") {
                        val = 0;
                            for (int y = 0; y < shape[0]; y++) {
                                val += arr[xy_to_index(x, y)];
                            }
                        }
                        new_arr.arr[x] = val;
                    }
                    return new_arr;
                }
                if (tp == "min") {
                    for (int x = 0; x < shape[1]; x++) {
                        val = arr[xy_to_index(x, 0)];
                        for (int y = 1; y < shape[0]; y++) {
                            if (arr[xy_to_index(x, y)] < val) {
                                val = arr[xy_to_index(x, y)];
                            }
                        }
                        new_arr.arr[x] = val;
                    }
                    return new_arr;
                }
                if (tp == "max") {
                    for (int x = 0; x < shape[1]; x++) {
                        val = arr[xy_to_index(x, 0)];
                        for (int y = 1; y < shape[0]; y++) {
                            if (arr[xy_to_index(x, y)] > val) {
                                val = arr[xy_to_index(x, y)];
                            }
                        }
                        new_arr.arr[x] = val;
                    }
                    return new_arr;
                }
            } else {
                if (lvl == 1) {
                    int val;
                    NDArray new_arr(shape[0], 1);
                    if (tp == "sum") {
                        for (int y = 0; y < shape[0]; y++) {
                            val = 0;
                            for (int x = 0; x < shape[1]; x++) {
                                val += arr[xy_to_index(x, y)];
                            }
                            new_arr.arr[y] = val;
                        }
                        return new_arr;
                    }
                    if (tp == "min") {
                        for (int y = 0; y < shape[0]; y++) {
                            val = arr[xy_to_index(0, y)];
                            for (int x = 0; x < shape[1]; x++) {
                                if (arr[xy_to_index(x, y)] < val) {
                                    val = arr[xy_to_index(x, y)];
                                }
                            }
                            new_arr.arr[y] = val;
                        }
                        return new_arr;
                    }
                    if (tp == "max") {
                        for (int y = 0; y < shape[0]; y++) {
                            val = arr[xy_to_index(0, y)];
                            for (int x = 0; x < shape[1]; x++) {
                                if (arr[xy_to_index(x, y)] > val) {
                                    val = arr[xy_to_index(x, y)];
                                }
                            }
                            new_arr.arr[y] = val;
                        }
                        return new_arr;
                    }
                } else {
                    throw "Error: wrong argument";
                }
            }
            return *this;
        }

        int sxn_check(std::string tp) {
            if (tp == "sum") {
                int val = 0;
                for (int i = 0; i < size; i++) {
                    val += arr[i];
                }
                return val;
            }
            if (tp == "min") {
                int val = arr[0];
                for (int i = 1; i < size; i++) {
                    if (arr[i] < val) {
                        val = arr[i];
                    }
                }
                return val;
            }
            if (tp == "max") {
                int val = arr[0];
                for (int i = 1; i < size; i++) {
                    if (arr[i] > val) {
                        val = arr[i];
                    }
                }
                return val;
            }
            return 0;
        }
};

std::ostream& operator<<(std::ostream &out, NDArray array) {
    out << array.display();
    return out;
}

int main(){
    //задание 1
    std::cout << "Задание 1" << std::endl;
    NDArray check1 (3, 1);
    std::cout << "Пустая матрица размера 3:1" << std::endl;
    std::cout << check1 << std::endl;
    check1.randoms(0, 50);
    std::cout << "Случайная матрица размера 3:1" << std::endl;
    std::cout << check1 << std::endl;
    check1.empty();
    std::cout << "Пустая матрица размера 3:1, созданная из случайной матрицы" << std::endl;
    std::cout << "(так как в матрице нет данных, она ссылается на случайные ячейки памяти)" << std::endl;
    std::cout << check1 << std::endl;
    std::cout << std::endl << std::endl << std::endl;
    std::cout << "!!!!!!!!!!!!!!!!!" << std::endl;

    //задание 2
    std::cout << "Задание 2" << std::endl;
    NDArray check2 (3, 2, 0);
    std::cout << "Матрица из нулей, заполненная при инициализации" << std::endl;
    std::cout << check2 << std::endl;
    check2.randoms(0, 50);
    std::cout << "Случайная матрица" << std::endl;
    std::cout << check2 << std::endl;
    check2.zeroes();
    std::cout << "Матрица из нулей, созданная из случайной матрицы с помощью метода zeroes" << std::endl;
    std::cout << check2 << std::endl;
    std::cout << std::endl << std::endl << std::endl;
    std::cout << "!!!!!!!!!!!!!!!!!" << std::endl;

    //задание 3
    std::cout << "Задание 3" << std::endl;
    NDArray check3 (3, 3, 1);
    std::cout << "Матрица из единиц, заполненная при инициализации" << std::endl;
    std::cout << check3 << std::endl;
    check3.randoms(0, 50);
    std::cout << "Случайная матрица" << std::endl;
    std::cout << check3 << std::endl;
    check3.ones();
    std::cout << "Матрица из единиц, созданная из случайной матрицы с помощью метода ones" << std::endl;
    std::cout << check3 << std::endl;
    std::cout << std::endl << std::endl << std::endl;
    std::cout << "!!!!!!!!!!!!!!!!!" << std::endl;

    //задание 4
    std::cout << "Задание 4" << std::endl;
    NDArray check4 (3, 4);
    check4.randoms(0, 50);
    std::cout << "Случайная матрица" << std::endl;
    std::cout << check4 << std::endl;
    std::cout << std::endl << std::endl << std::endl;
    std::cout << "!!!!!!!!!!!!!!!!!" << std::endl;

    //задание 5
    std::cout << "Задание 5" << std::endl;
    NDArray check51 (3, 5);
    check51.randoms(0, 50);
    NDArray check52 (3, 5);
    check52.randoms(0, 50);
    std::cout << "Оригиналы" << std::endl;
    std::cout << check51 << std::endl;
    std::cout << check52 << std::endl;
    std::cout << "Сумма" << std::endl;
    std::cout << check51 + check52 << std::endl;
    std::cout << "Разница" << std::endl;
    std::cout << check51 - check52 << std::endl;
    std::cout << "Произведение" << std::endl;
    std::cout << check51 * check52 << std::endl;
    std::cout << "Частное" << std::endl;
    std::cout << check51 / check52 << std::endl;
    std::cout << std::endl << std::endl << std::endl;
    std::cout << "!!!!!!!!!!!!!!!!!" << std::endl;

    //задание 6
    std::cout << "Задание 6" << std::endl;
    NDArray check61 (3, 6);
    check61.randoms(0, 50);
    NDArray check62 (6, 6);
    check62.randoms(0, 50);
    std::cout << "Оригиналы" << std::endl;
    std::cout << check61 << std::endl;
    std::cout << check62 << std::endl;
    std::cout << "Произведение" << std::endl;
    std::cout << check61.matmul(check62) << std::endl;
    std::cout << std::endl << std::endl << std::endl;
    std::cout << "!!!!!!!!!!!!!!!!!" << std::endl;

    //задание 7
    std::cout << "Задание 7" << std::endl;
    NDArray check7 (2, 7);
    check7.randoms(0, 50);
    std::cout << "Оригинал" << std::endl;
    std::cout << check7 << std::endl;
    std::cout << "Транспонированная" << std::endl;
    std::cout << check7.transpone() << std::endl;
    std::cout << std::endl << std::endl << std::endl;
    std::cout << "!!!!!!!!!!!!!!!!!" << std::endl;

    //задание 8
    std::cout << "Задание 8" << std::endl;
    NDArray check8 (3, 8);
    check8.randoms(0, 50);
    std::cout << "Оригинал" << std::endl;
    std::cout << check8 << std::endl;
    std::cout << "Минимумы" << std::endl;
    std::cout << check8.min() << std::endl << std::endl;
    std::cout << check8.min(0) << std::endl;
    std::cout << check8.min(1) << std::endl;
    std::cout << "Максимумы" << std::endl;
    std::cout << check8.max() << std::endl << std::endl;
    std::cout << check8.max(0) << std::endl;
    std::cout << check8.max(1) << std::endl;
    std::cout << "Средние" << std::endl;
    std::cout << check8.mean() << std::endl << std::endl;
    std::cout << check8.mean(0) << std::endl;
    std::cout << check8.mean(1);
}