#include <iostream>

struct Matrix {
    int *A;
    int n;

    void create(int size) {
        A = (int *) malloc(size * sizeof(int));
        n = size;
    }

    void set(int value, int i, int j) {
        if (i == j) {
            A[i - 1] = value;
        }
    }

    int get(int i, int j) {
        if (i == j) {
            return A[i-1];
        }
        return 0;
    }

    void display() {
        int i, j;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                std::cout << get(i+1, j+1) << " ";
            }
            std::cout << std::endl;
        }
    }
};

struct Triangle_matrix {
    int* A;
    int n;

    void create(int size) {
        A = (int *) malloc(size*(size + 1) / 2*sizeof(int));
        n = size;
    }

    void set(int value, int i, int j) {
        if (i >= j) {
            A[n*(j - 1) + (j - 2)*(j - 1) / 2 + i - j] = value;
        }
    }

    int get(int i, int j) {
        if (i >= j) {
            return A[n*(j - 1) + (j - 2)*(j - 1) / 2 + i - j];
        }
        return 0;
    }

    void display() {
        int i, j;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                std::cout << get(i+1, j+1) << " ";
            }
            std::cout << std::endl;
        }
    }
};

int main() {
    Matrix mat;
    mat.create(4);
    return 0;
}