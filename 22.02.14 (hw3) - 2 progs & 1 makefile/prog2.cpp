#include <iostream>
#include <cmath>

int main(){
    float fl1;
    int check = 0;
    float ans = 1;
    while (std::cin >> fl1) {
        if (check < 2) {
            std::cout << fl1 << ' ';
            ans *= sin(fl1);
        } else {
            ans = ans*2 + cos(fl1);
            std::cout << fl1 << std::endl << ans << std::endl << std::endl;
            ans = 1;
            check -= 3;
        }
        check++;
    }
    //system("pause");
    return 0;
}