#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>

int main(int argc, char *argv[]){
    if (argc != 2){
        std::cout << "ERROR" << std::endl;
    } else {
        std::ofstream fl1;
        fl1.open("out.txt");
        srand(time(0));
        int n = atoi(argv[1]);

        while (n > 0) {
            float a1 = (rand() % 630 - 315) / 100.0;
            float a2 = (rand() % 630 - 315) / 100.0;
            float a3 = (rand() % 630 - 315) / 100.0;
            fl1 << a1 << ' ' << a2 << ' ' << a3 << std::endl;
            n--;
        }
        fl1.close();
    }
    //system("pause");
    return 0;
}